import pyaudio # from http://people.csail.mit.edu/hubert/pyaudio/
import audioop
import sys
import math
import numpy as np
import scipy.fftpack as fft
import binascii
import socket
chunk    = 2084*2 # Change if too fast/slow, never less than 1024
device   = 2
p = pyaudio.PyAudio()
stream = p.open(format = pyaudio.paInt16,
                channels = 1,
                rate = 44100,
                input = True,
                frames_per_buffer = chunk,
                input_device_index = device)
mv=0
sub_strip=np.linspace(0,256,25)-256
while 1:
    try:
        S=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        S.settimeout(1)
        S.connect(("192.168.2.120",31337))
        print("Connected")
        while 1:
            data = np.fromstring(stream.read(chunk),np.int16)/2**15
            data = fft.rfft(data)
            data = np.abs(data)[1:]
            freqs = fft.rfftfreq(chunk,d=1/44100)[1:]
            data*=freqs
            data=data/freqs[-1]
            data*=256
    
            bass=freqs<500
            bass=data[bass].mean()
            bass=int(min(bass,512))
            bass=sub_strip+bass
            bass[bass<0]=0
            bass[bass>255]=255
            bass=bytes(bass.astype("uint8"))
            bass=bass+bass[::-1]
            
            mid=np.logical_and(500<freqs,freqs<2000)
            mid=data[mid].mean()
            mid=int(min(mid,512))
            mid=sub_strip+mid
            mid[mid<0]=0
            mid[mid>255]=255
            mid=bytes(mid.astype("uint8"))
            mid=mid+mid[::-1]
            
            high=2000<freqs
            high=data[high].mean()
            high=int(min(high,512))
            high=sub_strip+high
            high[high<0]=0
            high[high>255]=255
            high=bytes(high.astype("uint8"))
            high=high+high[::-1]
            
            data=[]
            for v in zip(bass,mid,high):
                data+=v
            S.send(bytes(data))
    except Exception as e:
        print("Error:",e)
        continue
