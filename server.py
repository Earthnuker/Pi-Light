import asyncore
import binascii
import spidev
NLEDS=50
SIZE=NLEDS*3*8
spi=spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=int(8e6) # 8MHz
def make_color(R,G,B):
    BIT_VALS=[
            0b11000000,
            0b11111100,
    ]
    R_L=[]
    G_L=[]
    B_L=[]
    for i in range(8):
            R_L.insert(0,BIT_VALS[(R>>i)&1])
            G_L.insert(0,BIT_VALS[(G>>i)&1])
            B_L.insert(0,BIT_VALS[(B>>i)&1])
    return R_L+G_L+B_L
class EchoHandler(asyncore.dispatcher_with_send):
    def handle_read(self):
        data = self.recv(SIZE)
        buffer=[]
        if data:
            data=list(data)
            if not len(data)%3==0:
                return
            while data:
                R=data.pop(0)
                G=data.pop(0)
                B=data.pop(0)
                buffer+=make_color(R,G,B)
        if buffer:
            spi.writebytes(buffer)
class EchoServer(asyncore.dispatcher):
    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket()
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(1)
    def handle_accepted(self, sock, addr):
        print('Incoming connection from {}:{}'.format(*addr))
        handler = EchoHandler(sock)
    def handle_close(self):
        print("bye")
server = EchoServer('0.0.0.0', 31337)
asyncore.loop()