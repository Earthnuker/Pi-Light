# Pi-Light


server.py: Server to run on Raspberry-Pi, listens on TCP:31337

ambilight.py: really hacky kind of Ambilight-Clone

music.py: makes LED-Strips light up as a VU-Meter (bass=red,mids=green,highs=blue)


## Protocol:

send RGB Triplets to the Server as raw bytes

so for a 3 LED-Strip you'd send \xff\x00\x00\xff\x00\x00\xff\x00\x00 to turn it completely red