import socket
import binascii
import math as M
import time
import numpy as np
from PIL import ImageGrab
from scipy.signal import resample
NLEDS=50
def get_col():
    res=np.array(ImageGrab.grab())/255
    res=res.mean(0)
    res=np.maximum(0,resample(res,NLEDS))
    res=res**1.5
    res[:,0]*=255.0
    res[:,1]*=240.0
    res[:,2]*=220.0
    #res*=0.5
    res=res[::-1,:]
    return res.astype("uint8")
while 1:
    try:
        S=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        S.settimeout(1)
        S.connect(("192.168.2.120",31337))
        print("Connected")
        while 1:
            data=bytes(get_col())
            S.send(data)
            #time.sleep(0.1)
    except Exception as e:
        print("Error:",e)
        continue